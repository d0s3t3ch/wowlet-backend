# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2022, The Monero Project.
# Copyright (c) 2022, dsc@xmr.pm

from typing import List
from dateutil.parser import parse

import settings
from wowlet_backend.utils import httpget
from wowlet_backend.tasks import WowletTask


class YellWowTask(WowletTask):
    """Fetches yellwowpages usernames/addresses"""
    def __init__(self, interval: int = 3600):
        super(YellWowTask, self).__init__(interval)

        self._cache_key = "yellwow"
        self._cache_expiry = self.interval * 10

        self._websocket_cmd = "yellwow"

        self._http_endpoint = "https://yellow.wownero.com/api/user/"

    async def task(self) -> List[dict]:
        blob = await httpget(self._http_endpoint)
        if not isinstance(blob, list) or not blob:
            raise Exception(f"Invalid JSON response for {self._http_endpoint}")
        return blob
