import re, sys, os
from datetime import datetime
import time

from wowlet_backend.factory import broadcast

HISTORY = []


async def add_chat(message: str, balance: float = 0):
    global HISTORY
    item = {
        "message": message,
        "balance": balance,
        "date": int(time.time())
    }

    HISTORY.append(item)
    if len(HISTORY) >= 25:
        HISTORY = HISTORY[:25]

    await broadcast.put({
        "cmd": "trollEntry",
        "data": item
    })

